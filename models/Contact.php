<?
class Contact extends SFModel {
	protected $relations = array(
		'Tasks' => 'Task',
		'Events' => 'Event',
	);
	protected $Tasks;
	protected $Events;

	public function getStateEmails($state) {
		return $this->query('SELECT Id, FirstName, LastName, Email FROM Contact Where AccountId IN(SELECT id FROM Account WHERE (ShippingState=\''.$state.'\' OR BillingState=\''.$state.'\') AND Type IN(\'Prospect\')) AND Email !=\'\'');
	}
}
