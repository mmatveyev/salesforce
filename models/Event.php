<?php
class Event extends Activity {
	protected function _getPlannedCount() {
		$res = $this->client()->query('SELECT OwnerId, COUNT(Id) FROM '.$this->getName().' WHERE ActivityDate>=TOMORROW GROUP BY OwnerId');
		return $this->parseStatResult($res);
	}

}
