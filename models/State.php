<?php
class State extends MysqlModel {
	private $table;
	protected static $db;
	protected $dbConfigDomain = 'salesforceDb';

	public function getCollection($country) {
		return $this->collection('c.iso="'.$country.'"');
	}

	public function prepareCollectionQuery($arg = array()) {
		return 'SELECT a.*, a.old_code as Code FROM countries c, states a WHERE c.id=a.country_id AND '.$arg['cond'].' ORDER BY a.name';
	}

	public function getByCode($country, $state) {
		$res = $this->collection('c.iso="'.$country.'" AND a.old_code="'.$state.'"');
		// ToDo: Add reaction on multiple choices
		if (!is_array($res)) {
			return null;
		}
		return $res[0];
	}

	public function getByPhone($phone) {
		return '';
		//$phoneUtil = libphonenumber\PhoneNumberUtil::getInstance();
		//$number = $phoneUtil->parseAndKeepRawInput($phone, "CA");
		$nationalSignificantNumber = $phoneUtil->getNationalSignificantNumber($number);

		$areaCodeLength = $phoneUtil->getLengthOfGeographicalAreaCode($number);
		if ($areaCodeLength > 0) {
			$areaCode = substr($nationalSignificantNumber, 0, $areaCodeLength);
		} else {
			return false;
		}

		$rows = $this->select('SELECT s.*, s.old_code AS code FROM states s LEFT JOIN phone_codes ph ON ph.state_id = s.id WHERE ph.code='.$areaCode);
		return $rows[0];
	}

	public function find($arg = array()) {
		// ToDo: Remove old code, use ISO values.
		$arg = array_merge(array(
			'CountryName' => '',
			'Statename' => '',
			'CountryCode' => '',
			'StateCode' => '',
			), $arg);
		$countryNameCond = 'c.name="'.$arg['CountryName'].'"';
		$stateNameCond = 's.name="'.$arg['StateName'].'"';
		$countryCodeCond = 's.country_iso2="'.$arg['CountryCode'].'"';
		$stateCodeCond = '(s.code="'.$arg['StateCode'].'" OR s.old_code="'.$arg['StateCode'].'")';
		$rows = $this->select('SELECT s.*, s.old_code AS code FROM states s 
			JOIN countries c ON c.id=s.country_id
			WHERE (('.$countryNameCond.' OR '.$countryCodeCond.') AND ('.$stateNameCond.' OR '.$stateCodeCond.'))
			OR ((c.id=226 OR c.id=38) AND ('.$stateCodeCond.' OR '.$stateNameCond.'))
			ORDER BY ('.$countryNameCond.') DESC, ('.$countryCodeCond.') DESC
			');
		return $rows;
	}

	public function getPSTOffset($date) {
		$date = new DateTime($date);

		$localTZ = new DateTimeZone("America/Vancouver");
		if (!$this->timezone){
			__to_log('Unknown timezone', $this);
			return '';
		}
		$remoteTZ = new DateTimeZone($this->timezone);

		$localOffset = $localTZ->getOffset($date)/3600;
		//ToDo: Hardcode. Resove it.
		$currentZoneLabel = $localOffset == -8? 'PST' : 'PDT';

		$remoteOffset = $remoteTZ->getOffset($date)/3600;
		$PSTOffset = $remoteOffset - $localOffset;

		if ($PSTOffset == 0) {
			return '';
		}
		if ($PSTOffset>0) {
			return $currentZoneLabel.' +'.$PSTOffset;
		} else {
			return $currentZoneLabel.' '.$PSTOffset;
		}
	}

}
