<?php
class Task extends Activity {
	public function getTodaysEmailCount() {
		$res = $this->client()->query('SELECT OwnerId, COUNT(Id) FROM Task WHERE CreatedDate='.TIME_FRAME.' AND Type=\'Email\' GROUP BY OwnerId');
		return $this->parseStatResult($res);
	}
	public function getOverdueCount() {
		$res = $this->client()->query('SELECT OwnerId, COUNT(Id) FROM '.$this->getName().' WHERE Status!=\'Completed\' AND Status!=\'Held\' AND ActivityDate<'.TIME_FRAME.' GROUP BY OwnerId');
		return $this->parseStatResult($res);
	}
	public function getTodaysEachCount($arg = array()) {
		if (array_key_exists('cond', $arg)) {
			$cond = $arg['cond'];
		}
		if (isset($cond)) {
			$cond = ' AND '.$cond;
		} else {
			$cond = '';
		}
		$res = $this->client()->query('SELECT OwnerId, COUNT(Id) FROM '.$this->getName().' WHERE (Status=\'Completed\' OR Status=\'Held\') AND ActivityDate='.TIME_FRAME.$cond.' GROUP BY OwnerId');
		return $this->parseStatResult($res);
	}

	protected function _getPlannedCount() {
		$res = $this->client()->query('SELECT OwnerId, COUNT(Id) FROM '.$this->getName().' WHERE Status!=\'Completed\' AND Status!=\'Held\' AND ActivityDate>='.TIME_FRAME.' GROUP BY OwnerId');
		return $this->parseStatResult($res);
	}
}
