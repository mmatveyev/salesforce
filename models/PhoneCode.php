<?php
/*
 * How to fill database:
 * 1 http://www.numberingplans.com/?page=dialling&sub=areacodes
 * 2 Select country
 * 3 Paste HTML source of the list to vim buffer
 * 4 macros JJJJJJJJJJjkJJ^M2ddjjk<< throught all rows
 * 5 replaces:
 * %s/^<tr[^>]*>[^>]*>[^>]*>[^>]*>//g
 * %s/<font[^>]*>[^>]*>/ /g
 * %s/<[^>]*>[^>]*>[^>]*> (1)/%%/g
 * %s/<.*$//g
 * %s/^\(.*\)%%\(.*\)$/INSERT INTO phone_codes(state_id, country_id, code) SELECT id, country_id, \2 FROM states WHERE country_id=COUNTRY_ID AND name="\1";
 * */
class PhoneCode extends MysqlModel {
	private $table='phone_codes';
	protected static $db;
	protected $dbConfigDomain = 'salesforceDb';
}
