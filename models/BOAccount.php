<?php
class BOAccount extends BOModel {
	private $table = 'accounts';

	public function getUnsyncronizedSF() {
		$rows = $this->collection('a.synchronized_at<a.updated_at');
		return $rows;
	}

	public function toSFAccount() {
		if (!$this->sf_reference) {
			return null;
		}
		$data = array(
			'Id' => $this->sf_reference,
		);
		switch($this->status) {
		case 'active':
			if ($this->seller_id) {
				$data['Type'] = 'Reseller';
			} else {
				$data['Type'] = 'Customer';
			}
			break;
		case 'inactive':
			$data['Type'] = 'ExCustomer';
			break;
		}
		return new Account($data);
	}

	public function setSynchronized($SFId) {
		self::sql('UPDATE '.$this->table.' SET synchronized_at=NOW() WHERE LEFT(sf_reference, 15)="'.SFModel::getId($SFId).'"');
	}

	protected function prepareCollectionQuery($arg) {
		return 'SELECT a.*, a_s.symbol AS status
			FROM '.$this->table.' AS a
			LEFT JOIN account_statuses a_s ON a_s.id=a.account_status_id
			WHERE '.$arg['cond'].
			' AND sf_reference IS NOT NULL AND sf_reference<>""';
	}

	public function getStateEmails($state) {
		$rows = $this->select(' select * from (
SELECT a.id account_id, u.first_name, u.last_name, u.email
			FROM users u
			JOIN account_addresses addr ON addr.user_id=u.id
			JOIN accounts a ON addr.account_id=a.id
			WHERE a.account_status_id=3
			AND a.customer_id IS NOT NULL
			AND a.deleted_at IS NULL
			AND addr.state_id=42818
      and u.email is not null
			ORDER BY addr.account_address_type_id DESC, addr.is_primary=0
) q
			GROUP BY q.account_id
			');
		return $rows;

	}
}
