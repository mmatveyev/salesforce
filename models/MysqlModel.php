<?php
class MysqlModel extends Model {
	private $table;
	protected static $db;
	protected $dbConfigDomain;

	public function __construct($arg = array()) {
		if ($this->dbConfigDomain) {
			if (!self::$db) {
				self::$db = new mysqli(
					Config::get($this->dbConfigDomain.'.host'),
					Config::get($this->dbConfigDomain.'.username'),
					Config::get($this->dbConfigDomain.'.password'),
					Config::get($this->dbConfigDomain.'.database'),
					Config::get($this->dbConfigDomain.'.port')
				);
				self::$db->set_charset('utf8');
				//self::$db->query('SET NAMES \'utf-8\'');
				if (self::$db->connect_error) {
					throw new Exception('Mysql connection error: '.self::$db->connect_error);
				}
			}
		}
		return parent::__construct($arg);
	}

	protected function collection($arg = array()) {
		if (is_string($arg)) {
			$arg = array('cond' => $arg);
		}
		$sql = $this->prepareCollectionQuery($arg);
		$rows = $this->select($sql);
		$ret = array();
		foreach ($rows as $row) {
			$ret[] = new $this($row);
		}
		return $ret;
	}
	protected function prepareCollectionQuery($arg) {
		return 'SELECT * FROM '.$this->table.' WHERE '.$arg['cond'];
	}
	protected function sql($query) {
		$rows = array();
		$res = self::$db->query($query);
		if (!$res) throw new Exception('Mysql error "'.self::$db->error.'" while running query "'.$query.'"');
		return $res;
	}
	protected function select($query) {
		$res = $this->sql($query);
		$rows = array();
		while ($row = $res->fetch_assoc()) {
			$rows[] = $row;
		}
		return $rows;
	}
}
