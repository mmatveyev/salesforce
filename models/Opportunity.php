<?php
class Opportunity extends SFModel {
	public function getTodaysOpenedCount() {
		$res = $this->client()->query('SELECT OwnerId, COUNT(Id), SUM(Amount) FROM Opportunity WHERE CreatedDate='.TIME_FRAME.' GROUP BY OwnerId');
		$res = $this->parseStatResult($res);
		foreach($res as $k => $v) {
			$res[$k] = $v[0].'($'.number_format((int) strip_tags($v[1])).')';
		}
		return $res;
	}
	
	public function getTodaysClosedCount() {
		$res = $this->client()->query('SELECT OwnerId, COUNT(Id), SUM(Amount) FROM Opportunity WHERE CloseDate='.TIME_FRAME.' AND IsClosed=TRUE AND IsWon=TRUE GROUP BY OwnerId');
		$res = $this->parseStatResult($res);
		foreach($res as $k => $v) {
			$res[$k] = $v[0].'($'.number_format((int) strip_tags($v[1])).')';
		}
		return $res;
	}

	protected function parseStatResult($arg) {
		$res = array();
		if ($arg->size) {
			foreach($arg->records as $rec) {
				$key = strip_tags(array_shift($rec->any));
				$res[$key] =array_values($rec->any);
			}
		}
		return $res;
	}

}
