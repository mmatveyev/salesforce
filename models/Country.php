<?php
class Country extends MysqlModel {
	private $table;
	protected static $db;
	protected $dbConfigDomain = 'salesforceDb';

	public function getCollection() {
		return $this->collection();
	}

	public function prepareCollectionQuery() {
		return 'SELECT *, iso AS ISO2 FROM countries ORDER BY (name="Canada") DESC, (name="United States") DESC, name ASC';
	}

}
