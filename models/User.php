<?php
class User extends SFModel {
	public static $list = array(
		'hscovel' => 		'005U0000000gEEv',
		'salesforce' => 	'005U0000000gFZQ',
		'chatter' => 		'005U0000000gEEq',
		'lsloan' => 		'005U0000000ga4U',
	);
	public function collection() {
		static $collection = null;
		if (!$collection) {
			$collection = $this->query('SELECT Id, Username, Name, Extension, Email, ReportGroup__c FROM User');
		}
		return $collection;
	}
	public function reportCollection() {
		static $reportCollection = null;
		if (!$reportCollection) {
			$reportCollection = $this->query("SELECT Id, Username, Name, Extension, Email, ReportGroup__c FROM User WHERE ReportGroup__c!=''");
		}
		return $reportCollection;
	}

	public static function getExtensions() {
		$extensions = array();
		$obj = new self();
		$users = $obj->collection();
		foreach ($users as $user) {
			if (isset($user->Extension)) {
				$extensions[$user->Id] = $user->Extension;
			}
		}
		return $extensions;
	}
}
