<?php
class AsteriskCall extends MysqlModel {
	protected static $db;
	protected $dbConfigDomain = 'asterisk';

	public function getTodaysEachOutboundCount() {
		$extensions = User::getExtensions();
		$rows = $this->getTodaysOutboundCount(join(',', $extensions));

		return self::formatEachCount($extensions, $rows);
	}

	public function getTodaysEachInboundCount() {
		$extensions = User::getExtensions();
		$rows = $this->getTodaysInboundCount(join(',', $extensions));

		return self::formatEachCount($extensions, $rows);
	}

	public function getTodaysUsersOutboundCount($ext) {
		$rows = $this->getTodaysOutboundCount($ext);
		if (!is_array($rows) || !count($rows)) {
			return '';
		}
		return $rows[0]['Count'].self::formatSum($rows[0]['Sum']);
	}

	public function getTodaysUsersInboundCount($ext) {
		$rows = $this->getTodaysInboundCount($ext);
		if (!is_array($rows) || !count($rows)) {
			return '';
		}
		return $rows[0]['Count'].self::formatSum($rows[0]['Sum']);
	}

	private function getTodaysOutboundCount($extensions) {
		// See developer's dummy analog in the end
		return $this->select('SELECT RIGHT(LEFT(channel,7),3) as EXT, count(*) as Count, sum(duration) as Sum
			FROM cdr
			WHERE RIGHT(LEFT(channel,7),3) in ('.$extensions.' ) 
				AND DATE(calldate)=CURDATE()
				AND disposition="ANSWERED"
				AND (dstchannel LIKE "%Fugu%" 
				OR dstchannel LIKE "%Voip%")
			GROUP BY EXT ');
	}
	
	private function getTodaysInboundCount($extensions) {
		return $this->select('SELECT RIGHT(LEFT(dstchannel,7),3) as EXT, count(*) as Count, sum(duration) as Sum
			FROM cdr
			WHERE RIGHT(LEFT(dstchannel,7),3) in ('.$extensions.' ) 
				AND DATE(calldate)=CURDATE()
				AND disposition="ANSWERED"
				AND LENGTH(src)>3
			GROUP BY EXT ');
	}

	private static function formatEachCount($extensions, $rows) {
		$users = array_flip($extensions);
		$ret = array();
		foreach($rows as $row) {
			$ret[$users[$row['EXT']]] = $row['Count'].'('.self::formatSum($row['Sum']).')';
		}
		return $ret;
	}

	private static function formatSum($val) {
		return ceil($val/60).':'.($val%60);
	}



	/*
	// Debug version
	private function getTodaysOutboundCount() {
		$rows = Array (
			0 => Array (
				'EXT' => 205,
				'Count' => 32,
				'Sum' => 87.3167,
			),
			1 => Array (
				'EXT' => 206,
				'Count' => 31,
				'Sum' => 90.5333,
			),
			2 => Array (
				'EXT' => 209,
				'Count' => 7,
				'Sum' => 9.3000,
			),
			3 => Array (
				'EXT' => 258,
				'Count' => 1,
				'Sum' => 4.0000,
			),
		);
		return $rows;
	}
	private function getTodaysInboundCount() {
		$rows = Array (
			0 => Array (
				'EXT' => 205,
				'Count' => 32,
				'Sum' => 87.3167,
			),
			1 => Array (
				'EXT' => 206,
				'Count' => 31,
				'Sum' => 90.5333,
			),
			2 => Array (
				'EXT' => 209,
				'Count' => 7,
				'Sum' => 9.3000,
			),
			3 => Array (
				'EXT' => 258,
				'Count' => 1,
				'Sum' => 4.0000,
			),
		);
		return $rows;
	}*/
}
