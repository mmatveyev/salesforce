<?
class Activity extends SFModel {
	// ToDo: Separate this to the Plan class
	private static $dailyPlan = array();

	public function plan($offset = 0) {
		if (!isset(self::$dailyPlan[$this->OwnerId])) {
			$date = self::getWorkDate(time()+60*60*24*$offset);
		} else {
			end(self::$dailyPlan[$this->OwnerId]);
			$date = key(self::$dailyPlan[$this->OwnerId]);
		}
		while ($this->isBusy($date)) {
			$date = self::getWorkDate($date+60*60*24);
		}
		$this->ActivityDate = date('Y-m-d', $date);
		if (!array_key_exists($this->OwnerId, self::$dailyPlan)) {
			self::$dailyPlan[$this->OwnerId] = array($date => 0);
		}
		if (!array_key_exists($date, self::$dailyPlan[$this->OwnerId])) {
			self::$dailyPlan[$this->OwnerId][$date] = 0;
		}
		self::$dailyPlan[$this->OwnerId][$date]++;
		$this->save();
	}

	private function isBusy($date) {
		return (
			array_key_exists($this->OwnerId, self::$dailyPlan) &&
			array_key_exists($date, self::$dailyPlan[$this->OwnerId]) &&
			self::$dailyPlan[$this->OwnerId][$date] >= 20);
	}

	private static function getWorkDate($date) {
		$weekday = date('N', $date);
		if ($weekday >5) {
			$date += 60*60*24*(8-$weekday);
		}
		return $date;
	}

	public function getPlannedCount() {
		$obj = new Task();
		$tasks = $obj->_getPlannedCount();
		$obj = new Event();
		$event = $obj->_getPlannedCount();
		foreach($event as $user => $val) {
			$tasks[$user] += $val;
		}
		return $tasks;
	}

}
