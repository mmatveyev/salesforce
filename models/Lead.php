<?php
class Lead extends SFModel {

	protected $relations = array(
		'Tasks' => 'Task',
		'Events' => 'Event',
	);
	protected $Tasks;
	protected $Events;
	protected $LastActivityDate;

	public function neglectedCollection($arg = array()) {
		if (array_key_exists('days', $arg)) {
			$days = $arg['days'];
		} else {
			$days = 5;
		}
		$conds = '';
		if (array_key_exists('OwnerId', $arg)) {
			$conds .= ' AND OwnerId=\''.$arg['OwnerId'].'\'';
		}
		$res = array();
		$rows = $this->query('SELECT Id, LastActivityDate, OwnerId, 
				(SELECT Id, ActivityDate FROM Tasks),
				(SELECT Id, ActivityDate FROM Events) 
				FROM Lead 
				WHERE (LastActivityDate<LAST_N_DAYS:'.$days.') 
				AND Status <> \'Closed - Converted\' AND Status <> \'Closed - Not Converted\'
				AND Neglected__c=FALSE
				'.$conds);

		foreach ($rows as $row) {
			if ($row->isNeglected($days)) {
				$res[] = $row;
			}
		}
		return $res;
	}

	public function getTodaysNewCount() {
		$res = $this->client()->query('SELECT OwnerId, COUNT(Id) FROM Lead WHERE CreatedDate='.TIME_FRAME.' GROUP BY OwnerId');
		return $this->parseStatResult($res);
	}

	public function createNeglectedActivity() {
		$task = new Task(array(
			'Subject' => 'Follow-up, neglected',
			'Description' => 'Auto created Touch base',
			'CallDurationInSeconds' => 15*60,
			'Status' => 'Not Started',
			'WhoId' => $this->Id,
			'OwnerId' => $this->OwnerId,
		));
		$task->plan(1);
	}

	public function getCollection($arg) {
		return $this->collection($arg);
	}

	public function findAddress($data = array()) {
		// First - phone. It's prefered
		$stateObj = new State();
		if (array_key_exists('Phone', $data)) {
			$state = $stateObj->getByPhone($data['Phone']);
			if (is_array($state) && !empty($state['code'])) {
				$data['Country'] = $state['country_iso2'];
				$data['State'] = $state['code'];
				$stateObj = new State($state);
				$data['PSTOffset'] = $stateObj->getPSTOffset(date('Y-m-d'));
				return $data;
			}
		}
		// Find by Country or state name or code
		$arg = array();
		if (!empty($data['CountryName'])) {
			$arg['CountryName'] = $data['CountryName'];
		}
		if (!empty($data['StateName'])) {
			$arg['StateName'] = $data['StateName'];
		}
		if (!empty($data['Country'])) {
			$arg['CountryCode'] = $data['Country'];
		}
		if (!empty($data['State'])) {
			$arg['StateCode'] = $data['State'];
		}
		$state = $stateObj->find($arg);
		if (is_array($state) && count($state) && !empty($state[0]['code'])) {
			$data['Country'] = $state[0]['country_iso2'];
			$data['State'] = $state[0]['code'];
			$stateObj = new State($state);
			$data['PSTOffset'] = $stateObj->getPSTOffset(date('Y-m-d'));
			return $data;
		}
		// Nothing found. Return unchanged data
		__to_log('Unknown state', $data);
		return $data;
	}
}
