<?
class Account extends SFModel {
	protected $relations = array(
		'Tasks' => 'Task',
		'Events' => 'Event',
	);
	protected $Tasks;
	protected $Events;

	public function neglectedCollection() {
		$res = array();
		$rows = $this->query('SELECT Id, LastActivityDate, Type, OwnerId, 
				(SELECT Id, ActivityDate FROM Tasks),
				(SELECT Id, ActivityDate FROM Events) 
				FROM Account 
				WHERE (LastActivityDate<LAST_N_DAYS:60 OR LastActivityDate=NULL) 
				AND (Type=\'Prospect\' OR Type=\'Customer - Direct\' OR Type=\'Pending\')');
		foreach ($rows as $row) {
			if ($row->isNeglected(60)) {
				$res[] = $row;
			}
		}
		return $res;
	}

	public function createNeglectedActivity() {
		$task = new Task(array(
			'Subject' => 'Follow-up, neglected',
			'Decription' => 'Auto created Touch base',
			'CallDurationInSeconds' => 15*60,
			'Status' => 'Not Started',
			'WhatId' => $this->Id,
			'OwnerId' => $this->OwnerId,
		));
		$task->plan(30);
	}

	public function toBOAccount() {
		return new BOAccount(array('sf_reference' => $thiis->Id));
	}
}
