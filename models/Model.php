<?php
class Model {
	public function __construct($arg = array()) {
		if (count($arg)) {
			foreach ($arg as $k => $v) {
				$this->$k = $v;
			}
		}
	}
}
