<?php
class SFModel extends Model {
   // http://www.fishofprey.com/2011/09/obscure-salesforce-object-key-prefixes.html
	protected $relations = array();
	private static $prefixes = array(
		'Account' => '001',

		'AccountContactRole' => '02Z',
		'AccountShare' => '00r',
		'AdditionalNumber' => '04m',
		'ApexClass' => '01p',
		'ApexComponent' => '099',
		'ApexLog' => '07L',
		'ApexTestQueueItem' => '709',
		'ApexTestResult' => '07M',
		'ApexPage' => '066',
		'ApexTrigger' => '01q',
		'Approval' => '806',
		'Asset' => '02i',
		'AssignmentRule' => '01Q',
		'AsyncApexJob' => '707',
		'Attachment' => '00P',
		'BrandTemplate (Letterhead)' => '016',
		'BusinessHours' => '01m',
		'BusinessProcess' => '019',
		'CallCenter' => '04v',
		'Campaign' => '701',
		'CampaignMember' => '00v',
		'CampaignMemberStatus' => '01Y',
		'CampaignShare' => '08s',
		'Case' => '500',
		'CaseComment' => '00a',
		'CaseContactRole' => '03j',
		'CaseShare' => '01n',
		'CaseSolution' => '010',
		'CategoryData' => '02o',
		'CategoryNode' => '02n',
		'Community' => '09a',
		'Contact' => '003',
		'ContactShare' => '03s',
		'Contract' => '800',
		'ContractContactRole' => '02a',
		'CronTrigger' => '08e',
		'Dashboard' => '01Z',
		'DashboardComponent' => '01a',
		'Document' => '015',
		'DocumentAttachmentMap' => '05X',
		'EmailServicesAddress' => '093',
		'EmailServicesFunction' => '091',
		'EmailStatus' => '018',
		'EmailTemplate' => '00X',
		'Event' => '00U',
		'EventAttendee' => '020',
		'FiscalYearSettings' => '022',
		'Folder' => '00l',
		'ForecastShare' => '608',
		'Group' => '00G',
		'GroupMember' => '011',
		'Holiday' => '0C0',
		'Idea' => '087',
		'IdeaComment' => '00a',
		'Lead' => '00Q',
		'LeadShare' => '01o',
		'LoginHistory' => '0Ya',
		'MailmergeTemplate' => '01H',
		'Note' => '002',
		'Opportunity' => '006',
		'OpportunityCompetitor' => '00J',
		'OpportunityContactRole' => '00K',
		'OpportunityHistory' => '008',
		'OpportunityLineItem' => '00k',
		'OpportunityLineItemSchedule' => '00o',
		'information about the quantity, revenue distribution, and delivery dates for a particular OpportunityLineItem.' => 'Represents',
		'Price Books, and Schedules Overview' => 'Products,',
		'OpportunityShare' => '00t',
		'OrgWideEmailAddress' => '0D2',
		'Organization' => '00D',
		'Partner' => '00I',
		'OpportunityPartner, which "is automatically created when a Partner object is created for a partner relationship between an account and an opportunity"' => 'Also',
		'OpportunityTeamMember - "This object is available only in organizations that have enabled team selling."' => '00q',
		'Period' => '026',
		'Pricebook2' => '01s',
		'PricebookEntry' => '01u',
		'ProcessInstance' => '04g',
		'ProcessInstanceStep' => '04h',
		'ProcessInstanceWorkitem' => '04i',
		'Product2' => '01t',
		'Profile' => '00e',
		'QueueSobject' => '03g',
		'RecordType' => '012',
		'Report' => '00O',
		'Scontrol' => '01N',
		'SelfServiceUser' => '035',
		'Solution' => '501',
		'StaticResource' => '081',
		'Task' => '00T',
		'User' => '005',
		'UserLicense' => '100',
		'UserPreference' => '03u',
		'UserProfileFeed' => '0D5',
		'UserRole' => '00E',
		'UserTeamMember' => '00p',
		'UserAccountTeamMember' => '01D',
		'Vote' => '083',
		'WebLink - Custom Button or Link' => '00b',
		'View' => '00B',
		'EntitySubscription' => '0E8',
		'FeedItem or NewsFeed or UserProfileFeed' => '0D5',
		'ContentWorkspace' => '058',
		'ContentWorkspaceDoc' => '059',
		'ContentDocument' => '069',
		'ContentVersion' => '068',
		'Package - being built in the developer org' => '033',
		'Install Package' => '04t',
		'Installed Package' => '0A3',
		'Weekly Data Export' => '092',
		'Import Queue' => '00S',
		'Sandbox' => '07E',
		'Apex Test Result' => '07M',
		'Change Set' => '0A2',
		'Inbound Change Set?' => '0EP',
		'Custom Field Definition' => '00N',
		'Connection - Salesforce to Salesforce' => '0BM',
		'Visualforce Tab' => '01r',
		'Custom Label' => '101',
		'Validation Rule' => '03d',
		'Workflow Rule' => '01Q',
		'Empty Key' => '000',
		'Quote' => '0Q0',
		'Custom Object' => '01I',
		'AsyncResult, DeployResult' => '04s',
		'Page Layout' => '00h',
		'Data from Uninstalled Packages? A zip containing CSVs.' => '082',
		'Outbound Notification Id' => '04l',
		'Portal Id' => '060',
		'TraceFlag' => '7tf',
		'Site' => '0DM',
		'SetupEntityAccess' => '0J0',
	);


	public function __construct($arg = array()) {
		foreach($this->relations as $k => $v) {
			$this->$k = array();
		}
		if (count($arg)) {
			foreach ($arg as $k => $v) {
				if (is_object($v)) {
					if (array_key_exists($k, $this->relations)) {
						$obj = new $this->relations[$k];
						$v = $obj->parseResponce($v);
					}
				}
				$this->$k = $v;
			}
		}
	}

	protected function isNeglected($period) {
		$period = 60*60*24*$period;
		$activities = array_merge($this->Tasks, $this->Events);
		foreach($activities as $activity) {
			if (time() - strToTime($activity->ActivityDate) < $period) {
				return false;
			}
		}
		return true;
	}

	protected function query($soql) {
		$rows = array();
		$res = $this->client()->query($soql);
		return $this->parseResponce($res);
	}

	protected function parseResponce($arg) {
		$rows = array();
		$done = false;
		if ($arg->size>0) {
			while (!$done) {
				if (is_object($arg->records)) {
					$arg->records = array($arg->records);
				}
				if (is_array($arg->records)) {
					foreach ($arg->records as $res) {
						$rows[] = new $this($res);
					}
				}
				if ($arg->done) {
					$done = true;
				} else {
					$arg = $this->client()->queryMore($arg->queryLocator);
				}
			}
		}
		return $rows;
	}

	public function get($id, $fields) {
		$res = $this->client()->retrieve($fields, $this->getName(), array($id));
		return new $this($res);
	}
	private function getKeyPrefix() {
		return self::$prefixes[$this->getName()];
	}

	public static function getType($id) {
		$key = substr($id, 0, 3);
		$values = array_flip(self::$prefixes);
		return $values[$key];
	}

	public static function getId($id) {
		return substr($id, 0, 15);
	}

	public function id() {
		return self::getId($this->Id);
	}

	protected function client() {
		global $SFClient;
		if (!$SFClient) {
			$SFClient = SalesForce::getInstance();
		}
		return $SFClient;
	}

	public function getName() {
		return get_class($this);
	}

	public function save() {
		//clean up relations
		$tmp = $this->hidePrivateProperties();
		if (isset($this->Id)) {
			$this->client()->update($this);
		} else {
			$this->client()->insert($this);
		}
		$this->restorePrivateProperties($tmp);
	}

	public function hidePrivateProperties() {
		$reflect = new ReflectionClass($this);
		$fields = array();
		$props = $reflect->getProperties(ReflectionProperty::IS_PRIVATE | ReflectionProperty::IS_PROTECTED);
		foreach ($props as $prop) {
			$name = $prop->getName();
			$fields[$name] = $this->$name;
			unset($this->$name);
		}
		return array('fields' => $fields, 'props' => $props);
	}
	private function restorePrivateProperties($arg) {
		foreach($arg['fields'] as $k => $field) {
			$this->$k = $field;
		}
	}

	protected function parseStatResult($arg) {
		$res = array();
		if ($arg->size) {
			foreach($arg->records as $rec) {
				if(count($rec->any) > 2) {
					mail('mmatveyev@positrace.com', 'Strange parse', print_r($rec, 1));
				}
				$key = strip_tags(array_shift($rec->any));
				$res[$key] = array_shift($rec->any);
			}
		}
		return $res;
	}

	protected function collection($arg = array()) {
		if (is_string($arg)) {
			$arg = array('cond' => $arg);
		}
		if (is_array($arg['cond'])) {
			$arg['cond'] = implode(' AND ', $arg['cond']);
		}
		return $this->query('SELECT Id, OwnerId FROM '.$this->getName().' WHERE '.$arg['cond']);
		
	}

}
