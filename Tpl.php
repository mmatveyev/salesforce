<?php
class Tpl {
	public static function parse($tpl, $args) {
		extract($args);
		ob_start();
		include('tpl/'.$tpl);
		return ob_get_clean();
	}

	public static function display($tpl, $args) {
		echo self::parse($tpl, $args);
	}
}
