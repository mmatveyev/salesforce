<?php
class Workflow extends Controller {
	private $responce;
	public function __construct($arg) {
		$this->responce = new stdClass();
		parent::__construct($arg);
	}

	/*
	public function test() {
		$obj = new stdClass();
		$obj->Id = '00TU000000NOXuBMAX';
		$obj->AccountId = '001U000000TLGGVIA5';
		$obj->ActivityDate = '2013-03-20';
		$obj->CreatedById = '005U0000000gEF0IAM';
		$obj->CreatedDate = '2013-03-13T15:48:24.000Z';
		$obj->CreatedDate__c = '2013-03-13T15:48:24.000Z';
		$obj->IsArchived = '';
		$obj->IsClosed = '';
		$obj->IsDeleted = '';
		$obj->IsRecurrence = '';
		$obj->IsReminderSet = '';
		$obj->LastModifiedById = '005U0000000gEF0IAM';
		$obj->LastModifiedDate = '2013-03-13T15:48:24.000Z';
		$obj->OwnerId = '005U0000000gEF0IAM';
		$obj->Priority = 'Normal';
		$obj->Status = 'Not Started';
		$obj->Subject = 'ON Expired Quote Follow up';
		$obj->SystemModstamp = '2013-03-13T15:48:24.000Z';
		$obj->Type = 'Call';
		$obj->WhatCount = '1';
		$obj->WhatId = '001U000000TLGGVIA5';
		$obj->WhoCount = '0';

		return $this->setTasksTimeZone($obj);
	}
	 */

	public function main() {
		switch($this->params[0]) {
		case 'taskCreated':
			$wsdl = 'new_task.wsdl';
			break;
		case 'leadCreated':
			$wsdl = 'new_lead.wsdl';
			break;
		default:
			throw new Exception('Unknown Workflow action: '.print_r($this->params, true));
		}
		$server = new SoapServer($wsdl);
		$server->setObject($this);
		$server->handle();
	}

	public function notifications($arg) {
		switch ($this->params[0]) {
		case 'taskCreated':
			$this->taskCreated($arg);
		case 'leadCreated':
			$this->leadCreated($arg);
		}
		$responce->Ack = true;
		return $responce;
	}

	private function taskCreated($rows) {
		if (!is_array($rows)) {
			$rows = array($rows);
		}
		if (count($rows) > 1) {
			mail('mmatveyev@gmail.com', 'Multiple calls came. Please check', print_r($rows, true));
		}
		foreach($rows as $row) {
			$call = $row->Notification->sObject;
			if (isset($call->WhoId) && SFModel::getType($call->WhoId)=='Lead' && (time() - strToTime($call->ActivityDate) < 60*60*24*5)) {
				$lead = new Lead(array('Id' => $call->WhoId, 'Neglected__c' => FALSE));
				$lead->save();
			}
			$this->setTasksTimeZone($call);
		}
	}

	private function setTasksTimeZone($call) {
		// Update State
		if (!empty($call->AccountId)) {
			// If there's AccountId it's beter to get state from Account
			$parentId = $call->AccountId;
			$obj = new Account();
			$obj = $obj->get($call->AccountId, 'ShippingState,BillingState,ShippingCountry,BillingCountry');
			if (!empty($obj->ShippingState)) {
				$state = $obj->ShippingState;
				$country = $obj->ShippingCountry;
			} else {
				$state = $obj->BillingState;
				$country = $obj->BillingCountry;
			}
		} else { // AccountId is empty
			if (isset($call->WhoId)) {
				$parentId = $call->WhoId;
			} else {
				$parentId = $call->WhatId;
			}
			if (!$parentId) {
				return false;
			}
			$type = SFModel::getType($parentId);
			$stateField = '';
			switch ($type) {
			case 'Contact':
				$stateField = 'MailingState';
				break;
			case 'Lead':
				$stateField = 'State';
				break;
			case 'Contract':
				$stateField = 'ShippingState';
				break;
			default: continue;
				/*
			case 'Account':
			case 'Asset':
			case 'Campaign':
			case 'Opportunity':
			case 'Product2':
			case 'Solution':
				 */
			}
			if (!$type) {
				__to_log('Unknown parent type in record:', $call);
				return false;
			}
			$obj = new $type;
			//ShippingState, ShippingCountry
			$countryField = str_replace('State', 'Country', $stateField);
			$obj = $obj->get($parentId, $countryField.', '.$stateField);
			$state = $obj->$stateField;
			$country = $obj->$countryField;
		}
		$stateObj = new State();
		if(!$country || !$state) {
			if ($state && !$country) {
				__to_log('Unknown country in task', $call);
			}
			return false;
		}
		$stateObj = $stateObj->getByCode($country, $state);
		if (is_object($stateObj)) {
			$offset = $stateObj->getPSTOffset($call->ActivityDate);
		} else {
			$offset = '';
			__to_log('Unknown code', array('country' => $country, 'state' => $state));
			__to_log('Data', $call);
			return false;
		}
		$task = new Task(array(
			'Id' => $call->Id,
			'State__c' => $state,
			'PSTOffset__c' => $offset,
		));
		$task->save();

	}

	private function leadCreated($rows) {
		if (!is_array($rows)) {
			$rows = array($rows);
		}
		if (count($rows) > 1) {
			mail('mmatveyev@gmail.com', 'Multiple calls came. Please check', print_r($rows, true));
		}
		foreach($rows as $row) {
			$lead = $row->Notification->sObject;

			if (isset($lead->Phone)) {
				$state = new State();
				$rec = $state->getByPhone($lead->Phone);
				$lead = new Lead(array(
					'Id' => $lead->Id,
					'Country' => $rec['country_iso2'],
					'State' => $rec['code'],
				));
				$lead->save();
			}
		}
	}
}
