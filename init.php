<?php
function autoloader($class) {
	$paths = array(
		'.',
		'models',
	);
	$class = str_replace('\\', '/', $class);

	foreach ($paths as $path) {
		$name = $path.'/'.$class.'.php';
		if (file_exists($name)) {
			include_once($name);
			return true;
		}
	}
	return false;
}
spl_autoload_register('autoloader');
