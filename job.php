<?php
chdir(dirname(__FILE__));
ini_set('soap.wsdl_cache_enabled', '0');
include_once('init.php');
class SFJob {

	public function __construct($action) {
		switch ($action) {
	/// Cron jobs ///
		case 'accounts_touch_base':
			return $this->accountsTouchBase();
			break;
		case 'neglected_leads':
			return $this->setNeglectedLeads();
			break;
		//SUG-76: Bo account status linked to SF
		case 'replicate_accounts':
			return $this->replicateAccounts();
			break;
		case 'activity_report':
			return $this->activityReport();
			break;

	/// CLI jobs ///
		//SUG-86: Make calls queue for Harvey
		case 'harvey_calls_queue':
			return $this->neglectedLeadsCallQueue();
		//SUG-86: Expot Accounts list
		case 'zemfira_accounts':
			return $this->zemfiraBcAccounts();
		default:
			throw new Exception('Unknown action: '.$action);
		}
	}

	/// Cron jobs ///

	private function accountsTouchBase() {
		$account = new Account();
		$rows = $account->neglectedCollection();
		foreach($rows as $row) {
			$row->createNeglectedActivity();
		}
	}

	private function replicateAccounts() {
		$boAccount = new BOAccount();
		$rows = $boAccount->getUnsyncronizedSF();
		foreach ($rows as $row) {
			$rec = $row->toSFAccount();
			if ($rec && !empty($rec->Type)) {
				$rec->save();
			}
		}
		$res = SalesForce::getInstance()->flush();

		foreach ($res as $account) {
			if ($account->success) {
				$boAccount->setSynchronized($account->id);
			} else {
				__to_log('Unsyncronized account', $account);
			}
		}
	}

	private function setNeglectedLeads() {
		$lead = new Lead();
		$rows = $lead->neglectedCollection(array('days' => 5));
		foreach($rows as $row) {
			$row->Neglected__c = TRUE;
			$row->save();
		}
	}

	private function activityReport() {
		//ToDo: Debug. Resolve it
		define ('TIME_FRAME','TODAY');
		$fieldsSales = array(
			'Calls made',
			'Calls held',
			'Overdue activities',
			'Tasks completed',
			'Planned activities',
			'New opportunities',
			'Closed opportunities',
			'New leads',
		);
		$fieldsBusinessDeveloper = array(
			'Calls made',
			'Calls held',
			'Tasks completed',
			'Pitch count',
			'New leads',
		);

		$obj = new User();
		$rows = $obj->reportCollection();
		$users = array();
		foreach ($rows as $row) {
			if ($row->id() != User::$list['salesforce'] && $row->id() != User::$list['chatter']) {
				$row->report = '';
				if (!isset($row->ReportGroup__c)) {
					$row->ReportGroup__c = '';
				}
				$fields = 'fields'.$row->ReportGroup__c;
				if (!empty($$fields)) {
					$row->reportFields = $$fields;
				} else {
					$row->reportFields = array();
				}
				if ($row->ReportGroup__c == 'Admin') {
					$totalsEmails[] = $row->Name.'<'.$row->Email.'>';
				}
				$users[$row->Id] = $row;
			}
		}
		# of Calls made, total minutes
		$obj = new AsteriskCall();
		$res['Calls made'] = $obj->getTodaysEachOutboundCount();
		$res['Calls held'] = $obj->getTodaysEachInboundCount();
		# of Emails logged in SF
		$obj = new Task();
		//$res['Emails logged'] = $obj->getTodaysEmailCount();
		# of overdue activities
		$res['Overdue activities'] = $obj->getOverdueCount();
		#Total tasks logged
		$res['Tasks completed'] = $obj->getTodaysEachCount();
		$res['Pitch count'] = $obj->getTodaysEachCount(array('cond' => 'Status=\'Pitched\''));
		# of planned activities
		$obj = new Activity();
		$res['Planned activities'] = $obj->getPlannedCount();
		# of new opportunities, total $ value
		$obj = new Opportunity();
		$res['New opportunities'] = $obj->getTodaysOpenedCount();
		# of closed opportunities, total $ value
		$res['Closed opportunities'] = $obj->getTodaysClosedCount();
		# new leads
		$obj = new Lead();
		$res['New leads'] = $obj->getTodaysNewCount();

		$totalsRep = '';
		foreach ($res as $name => $values) {
			$rowName = '<td style="border: 1px solid;">'.$name.'</td>';
			$totalsRep .= '<tr>'.$rowName;
			foreach ($users as $userId => $user) {
				if (!array_key_exists($userId, $values))
					$values[$userId] = '-';
				$value = '<td style="border: 1px solid;">'.$values[$userId].'</td>';
				if (in_array($name, $user->reportFields)) {
					$user->report .= '<tr>'.$rowName.$value.'</tr>'."\n";
				}
				$totalsRep .= $value;
			}
			$totalsRep.= '</tr>'."\n";
		}

		$headers = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
		$headers .= 'From: Salesforce Bot <salesforce@positrace.com>'."\r\n";
		$headers .= 'Cc: mmatveyev@positrace.com' . "\r\n";
		$body = '<html><head><title>Daily report</title></head><body><h2>'.date('F j, Y').'</h2><table style="border-collapse: collapse;">%s</table></body></html>';
		$totalsHead = '';
		foreach ($users as $user) {
			$totalsHead .= '<td style="border: 1px solid;">'.$user->Name.'</td>';
			if (count($user->reportFields)) {
				//mail('mmatveyev@positrace.com', 'Daily activity report for '.$user->Name, sprintf($body, $user->report), $headers);
				mail($user->Email, 'Daily report', sprintf($body, $user->report), $headers);
			}
		}
		$totalsRep = '<tr style="background: #888888;"><td style="border: 1px solid;">&nbsp;</td>'.$totalsHead.'</tr>'."\n".$totalsRep;
		$totalsEmails = implode(',', $totalsEmails);
		//$totalsEmails = 'mmatveyev@positrace.com';
		mail($totalsEmails, 'Overall daily activity report', sprintf($body, $totalsRep), $headers);
	}


	/// CLI jobs ///

	// Create tasks for Harvey's neglected leads, 20 per day.
	private function neglectedLeadsCallQueue() {
		// Job done. Lock it from 
		return false;
		$lead = new Lead();
		$rows = $lead->getCollection(array('cond' => array(
			'Neglected__c=TRUE',
			'OwnerId=\''.User::$list['hscovel'].'\'',
			'Status!=\'Closed - Converted\'',
			'Status!=\'Closed - Not Converted\'',
			'Status!=\'Recycled\'',
			'Status!=\'Duplicate\''))
		);
		foreach($rows as $row) {
			$row->createNeglectedActivity();
		}
		$res = SalesForce::getInstance()->flush();
		$task = new Task();
		$rows = $task->query('SELECT Id, WhoId FROM Task WHERE Subject=\'Follow-up, neglected\'');
		foreach($rows as $row) {
			$lead = new Lead(array('Id' => $row->WhoId, 'Neglected__c' => 'FALSE'));
			$lead->save();
		}
		$res = SalesForce::getInstance()->flush();
	}

	// Import accounts to csv
	private function zemfiraBcAccounts() {
		$obj = new BOAccount();
		$rows = $obj->getStateEmails('BC');
		$str = '';
		$f = fopen('export/BCEmails.csv', 'w+');
		foreach($rows as $row) {
			//$arr = array($row->first_name, $row->last_name, $row-> Email);
			fputcsv($f, $row);
		}
		fclose($f);
		echo 'Done';
	}
}

try {
	$job = new SFJob($argv[1]);
} catch (Exception $e) {
	//printr($e->getMessage());
	mail('mmatveyev@positrace.com', 'Cron error', $e->getMessage());
}
