jQuery.prototype.callablePhone = function() {
	return jQuery(this).each(function(){
		var val = jQuery(this).html();
		jQuery(this).html('<a href="callto://'+val.replace(/\D/g, '')+'">'+val+'</a>');

	});
}
jQuery(document).ready(function(){
	jQuery('td.PhoneNumberElement').callablePhone();
	jQuery('table.detailList td:contains("Phone")').next().children('div').callablePhone();
// SUG-104: Hide system fields
	jQuery('h3:contains("HiddenSection")').parent().hide().next().hide();
});
oldUpdateField = InlineEditField.SimpleField.prototype.updateReadElement;
InlineEditField.SimpleField.prototype.updateReadElement = function() {
	var res = oldUpdateField.call(this);
	if (this.initialHTML.indexOf('<a href="callto://')==0) {
		jQuery(this.readDiv).callablePhone().children('a').css('color', '#ff6a00');
}
}
