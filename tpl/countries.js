var countriesStr = '<?php echo $countriesStr; ?>';
//OverlayDialog.prototype.hide
var attributes = ['id', 'name', 'tabindex'];
jQuery.noConflict();
var updateOriginValue = function(el) {
	// Fill origin Edit with new value
	var elem = jQuery(el);
	var id = elem.attr('id');
	id=id.substr(0, id.length -1);
	jQuery('#'+id).val(elem.val());
}
drawStates__c = function (orig, country) {
	jQuery.post('<?php echo Config::get('system.baseUrl');?>/javascript', {action: 'states', country: country.val()}, function(data) {
		var selectId = orig.attr('id')+'_';
		jQuery('#'+selectId).remove();
		var value = orig.val();
		orig.after('<select id="'+selectId+'">'+data+'</select>');
		orig.hide();
		var select = jQuery('#'+selectId);
		select.val(value).change(function(){updateOriginValue(this)}).trigger('change');
	}, 'json');
}
function redrawStates__c() {
	updateOriginValue(this);
	var country = jQuery(this);

	var stateId=country.attr('id').replace('country', 'state');
	stateId=stateId.substr(0, stateId.length -1);
	drawStates__c(jQuery('#'+stateId), country);
}
drawCountries__c = function(orig) {
	var value = orig.val();
	var stateEdit = jQuery('#'+orig.attr('id').replace('country', 'state'));
	var selectId = orig.attr('id')+'_';
	if (jQuery('#'+selectId).length ==0) {
		orig.after('<select id="'+selectId+'">'+countriesStr+'</select>');
		orig.hide();
		var select = jQuery('#'+selectId);
		orig.on('change', function(){
			console.log('Changed');
			select.val(jQuery(this).val());
			select.trigger('change');
		});
		select.val(value).change(redrawStates__c).trigger('change');
	}
}
jQuery(document).ready(function(){
	var countries = jQuery('label:contains("Country")');
	if (countries.length) {
		for (var i=0; i<countries.length; i++) {
			drawCountries__c(jQuery('#'+jQuery(countries[i]).attr('for')));
		}
	}
});

oldInlineEditDialogShow = InlineEditDialog.prototype.show;
InlineEditDialog.prototype.show = function() {
	var res = oldInlineEditDialogShow.call(this);
	var countries = jQuery('label:contains("Country")');
	if (countries.length) {
		for (var i=0; i<countries.length; i++) {
			drawCountries__c(jQuery('#'+jQuery(countries[i]).attr('for')));
		}
	}
	return res;
}
oldCopyAddr = copyAddr;
copyAddr = function() {
	var res = oldCopyAddr.apply(this, arguments);
	jQuery('#'+jQuery('label:contains("Shipping Country")').attr('for')).trigger('change');
	jQuery('#'+jQuery('label:contains("Other Country")').attr('for')).trigger('change');
	return res;
}
