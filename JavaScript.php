<?php
class JavaScript extends Controller {
	protected $table = 'states';
	public function main() {
		header('Access-Control-Allow-Origin: *');
		if (array_key_exists('action', $_POST) && $_POST['action'] == 'states') {
			return $this->statesList();
		} else {
			return $this->commonScripts();
		}
	}
	private function commonScripts() {
		header('Content-type: text/javaScript');
		$this->countriesList();
		Tpl::display('common.js', array());
	}
	private function countriesList() {
		$country = new Country();
		$countriesStr = '<option value="">--Not specified--</option>';
		foreach ($country->getCollection() as $row) {
			$countriesStr .= '<option value="'.$row->ISO2.'">'.addslashes($row->name).'</option>';
		}
		return Tpl::display('countries.js', array(
			'countriesStr' => $countriesStr,
		));
	}

	private function statesList() {
		$str = '<option value="">--Not specified--</option>';
		if ($_POST['country']) {
			$obj = new State();
			$rows = $obj->getCollection($_POST['country']);
			foreach ($rows as $row) {
				$str .= '<option value="'.$row->Code.'">'.addslashes($row->name).'</option>';
			}
		}
		echo json_encode($str);
	}
}
