<?php
class Config {
	private static $config = array(
		'live' => array(
			'system' => array(
				'baseUrl' => 'https://salesforce.positrace.com',
			),
			'salesforce' => array(
				'username' => 'salesforce@positrace.com',
				'password' => 'SF12positraceT4Bs5TfejNF0vWehE0hVBy6s',
			),
			'salesforceDb' => array(
				'database' => 'salesforce',
				'username' => 'sugarcrm',
				'password' => 's000sw33t',
				'host' => '127.0.0.1',
				'port' => '3306',
			),
			'backoffice' => array(
				'database' => 'backoffice',
				'username' => 'mmatveyev',
				'password' => 'fkg7h4f3v6',
				'host' => '192.168.15.17',
				'port' => '3310',
			),
			'asterisk' => array (
				'host' => '192.168.15.11',
				'username' => 'sugarcrm',
				'password' => 'Nei2Giet',
				'database' => 'asteriskcdrdb',
				'port' => '3306',
				'tcdr' => 'asteriskcdrdb.cdr',
				'tusers' => 'asterisk.users',
				'ext_list' => "203, 204, 205, 206, 208, 257",
			),
		),
		'devel' => array(
			'system' => array(
				'baseUrl' => 'http://193.110.112.131:3080/~max/SF',
			),
			'salesforce' => array(
				'username' => 'salesforce@positrace.com',
				'password' => 'SF12positraceT4Bs5TfejNF0vWehE0hVBy6s',
			),
			'salesforceDb' => array(
				'database' => 'salesforce',
				'username' => 'devel',
				'password' => 'mysqladmin',
				'host' => '127.0.0.1',
				'port' => '3306',
			),
			'backoffice' => array(
				'database' => 'backoffice',
				'username' => 'devel',
				'password' => 'mysqladmin',
				'host' => 'localhost',
				'port' => '3306',
			),
			'asterisk' => array (
				'host' => '192.168.15.11',
				'username' => 'sugarcrm',
				'password' => 'Nei2Giet',
				'database' => 'asteriskcdrdb',
				'port' => '3306',
				'tcdr' => 'asteriskcdrdb.cdr',
				'tusers' => 'asterisk.users',
				'ext_list' => "203, 204, 205, 206, 208, 257",
			),
		),
	);
	private static $environment = '';
	private function getEnvironment() {
		if (isset($_SERVER['SERVER_ADDR']) && $_SERVER['SERVER_ADDR'] == '192.168.1.45') {
			self::$environment = 'devel';
		} 
		elseif (isset($_SERVER['TERM_PROGRAM']) && $_SERVER['TERM_PROGRAM'] = 'Apple_Terminal') {
			self::$environment = 'devel';
		} else {
			self::$environment = 'live';
		}
		return self::$environment;
	}
	public static function get($name) {
		if (!self::$environment) {
			self::getEnvironment();
		}

		$parts = explode('.', $name);
		$conf = self::$config[self::$environment];
		foreach ($parts as $part) {
			if (isset($conf[$part])) {
				$conf = $conf[$part];
			} else {
				throw new Exception('Config param not found: '.$name.' in environment "'.self::$environment.'"');
			}
		}
		return $conf;
	}
}
