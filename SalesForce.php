<?php
define("SOAP_CLIENT_BASEDIR", "phptoolkit/soapclient");
require_once (SOAP_CLIENT_BASEDIR.'/SforceEnterpriseClient.php');
class SalesForce  extends SforceEnterpriseClient {
	private $insertQueue = array();
	private $updateQueue = array();
	private $results = array();
	public function __construct() {
		parent::__construct();
		$this->createConnection(SOAP_CLIENT_BASEDIR.'/enterprise.wsdl.xml');
		$this->login(Config::get('salesforce.username'), Config::get('salesforce.password'));
	}
	public static function getInstance() {
		static $instance = null;
		if ($instance === null) {
			$instance = new SalesForce();
		}
		return $instance;
	}
	public function insert($arg) {
		$this->insertQueue[] = clone $arg;
		if (count($this->insertQueue)>=200) {
			$this->flushInsertQueue();
		}
	}
	public function update($arg) {
		$this->updateQueue[] = clone $arg;
		if (count($this->updateQueue)>=200) {
			$this->flushUpdateQueue();
		}
	}
	private function flushInsertQueue() {
		if (count($this->insertQueue)) {
			$tmp = $this->insertQueue;
			$this->insertQueue = array();
			$res = $this->_create($tmp);
			if (is_object($res)) {
				$res = array($res);
			}
			$this->results = array_merge($this->results, $res);
			return true;
		}
		return false;
	}
	private function flushUpdateQueue() {
		if (count($this->updateQueue)) {
			$tmp = $this->updateQueue;
			$this->updateQueue = array();
			$res = $this->_update($tmp);
			if (is_object($res)) {
				$res = array($res);
			}
			$this->results = array_merge($this->results, $res);
			return true;
		}
		return false;
	}
	public function flush() {
		$this->flushInsertQueue();
		$this->flushUpdateQueue();
		$res = $this->results;
		$this->results = array();
		return $res;
	}
	
	public function _create($sObjects, $type='') {
		foreach ($sObjects as &$sObject) {
			if (!$type) {
				$type = $sObject->getName();
			}
			$sObject = new SoapVar($sObject, SOAP_ENC_OBJECT, $type, $this->namespace);
		}
		$arg = $sObjects;

		/*
		echo 'SOAP creating: ';
		print_r(new SoapParam($arg, "sObjects"));
		return array();
		 */
		return parent::_create(new SoapParam($arg, "sObjects"));
	}
	public function _update($sObjects, $type = '') {
		foreach ($sObjects as &$sObject) {
			if ($type) {
				$type_ = $type;
			} else {
				$type_ = $sObject->getName();
			}
			$sObject = new SoapVar($sObject, SOAP_ENC_OBJECT, $type_, $this->namespace);
		}
		$arg->sObjects = $sObjects;
		return parent::_update($arg);
	}
	public function __destruct() {
		$this->flush();
	}
}
