<?php
include_once('init.php');
try {
	$parts = explode('/', $_GET['r']);
	$controller = array_shift($parts);
	switch($controller) {
	case 'javascript':
		$controller = new JavaScript();
		break;

	case 'workflow':
		$controller = new Workflow($parts);
		break;

	case 'service':
		$controller = new Service($parts);
		break;

	default:
		throw new Exception('Url not found: '.$_GET['r']);
	}
	$controller->main();
} catch (Exception $e) {
	__to_log('Exception', $e->getMessage());
	//mail('mmatveyev@positrace.com', 'Error', $e->getMessage());
}
